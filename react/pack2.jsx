import React, { useState } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useLazyQuery } from 'react-apollo'
import QUERY_VALUE from './query/collections.gql'
import QUERY_ORDER from './query/orderId.gql'
import $ from 'jquery'


const pack2 = () => {
  const CSS_HANDLES = [
    'mequieromorir',
    'container',
    'containerDesktop',
    'containerMobile',
    'blockLeft',
    'blockLeftMobile',
    'subBlockLeft',
    'blockCenter',
    'blockCenterMobile',
    'subBlockCenter',
    'blockRight',
    'blockRightMobile',
    'subBlockRight',
    'textBlock',
    'gender',
    'pasoUnoContainer',
    'pasoDosContainer',
    'pasoTresContainer',
    'genderContainer',
    'genderBtnContainer',
    'btngenderH',
    'btngenderM',
    'btngenderK',
    'circuloOn',
    'circuloOff',
    'textProduct',
    'promos',
    'seleccion',
    'finalizar',
    'back',
    'backContainer',
    'promo',
    'imgselection',
    'btntalla',
    'btnAzul',
    'promosM',
    'promosT',
    'promosH',
    'promosItem',
    'variants',
    'carrito',
    'product',
    'productContainer',
    'numero',
    'agregar',
    'sum',
    'rest',
    'input',
    'contTallas',
    'quality',
    'X',
    'price',
    'leyenda',
    'descuento',
    'divBtnAddCart',
    'containerprueba',
    'msjepack',
    'sectionContainer',
    'containerTeFaltan',
    'containerMath',
    'textPack',
    'btnPopup',
    'textPedido',
    'pasosContainer',
    'pasoOffContainer',
  ]

  const [loadOrder, datas] = useLazyQuery(QUERY_ORDER);

/*   if (window.attachEvent) {
    window.attachEvent('onload', chargeQuery);
  } else {
    if (window.onload) {
      var curronload = window.onload;
      var newonload = function (evt) {
        curronload(evt);
        chargeQuery(evt);
      };
      window.onload = newonload;
    } else {
      window.onload = chargeQuery;
    }
  } */

  function chargeQuery() {

    loadOrder();

    let orderForm = datas.data.orderForm.id
    let aprob = datas.called;
    console.log("apro",aprob);
    return [orderForm, aprob]
  }

  function addToCart() {
    tallaActive()
    let charge = chargeQuery()
    let dada = charge[0];
    /* console.log("dada",dada)
    console.log("charge",charge) */
    if (charge[1]) {
      $(".kayserltda-arma-pack-2-x-containerprueba").attr('style', '/*display: none*/')

    }
    let carrito = document.getElementById('carrito')
    console.log(carrito)
    let array = [];
    var productos = carrito.getElementsByClassName("kayserltda-arma-pack-2-x-prodItemID");
    var quantity = carrito.getElementsByClassName("kayserltda-arma-pack-2-x-prodCant");

    for (var i = 0; i < productos.length; i++) {

      array.push([productos[i].id, quantity[i].textContent])
    }
    console.log(array)
    let cuerpo = "{\"orderItems\":[";

    for (let j = 0; j < array.length; j++) {
      var id = array[j][0];
      var quantityy = array[j][1];
      console.log("datos", id, quantityy)
      cuerpo = cuerpo + "{\"quantity\":" + quantityy + ",\"seller\":\"1\",\"id\":\"" + id + "\"},"
    }
    console.log("cuerpo", cuerpo)
    cuerpo = cuerpo + "]}";
    fetch("/api/checkout/pub/orderForm/" + dada + "/items", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: cuerpo
    })
      .then(r => r.json())
      .then(data => console.log('data returned:', data));
}

  const [collect, setCollect] = useState("530");

  const [loadGreeting, { called, loading, data }] = useLazyQuery(QUERY_VALUE, { variables: { collect } });

  const [posi, setPosi] = useState(1);
  const [cantPromo, setPromo] = useState(1);

  let arreglo;

  let newdata = { data };

  if (data) {
    /* console.log("data products", data.products); */
    prueba2();
    
  }

  function tallaActive() {
    loadOrder();
    console.log("tallaactive")
    // Get the container element
    var btnContainer = document.getElementById('tallas');

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName("kayserltda-arma-pack-2-x-btntalla");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName(" kayserltda-arma-pack-2-x-active");

        // If there's no active class
        if (current.length > 0) {
          current[0].className = current[0].className.replace(" kayserltda-arma-pack-2-x-active", "kayserltda-arma-pack-2-x-btntalla");
        }

        // Add the active class to the current/clicked button
        this.className = " kayserltda-arma-pack-2-x-active";
      });
    }

    var btnContainer = document.getElementById('tallasm');

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName("kayserltda-arma-pack-2-x-btntalla");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName(" kayserltda-arma-pack-2-x-active");

        // If there's no active class
        if (current.length > 0) {
          current[0].className = current[0].className.replace(" kayserltda-arma-pack-2-x-active", "kayserltda-arma-pack-2-x-btntalla");
        }

        // Add the active class to the current/clicked button
        this.className = " kayserltda-arma-pack-2-x-active";
      });
    }
  }

  function MostrarVariantesProd(props) {
    let cod = props.codigo;
    return [cod]
  }
  let itemid;
  let nombre_prod;
  let prod_imgc;

  function MostrarDetalle(pos, e) {
    e.preventDefault()
    changeContainer();
    setPosi(pos);
    let prod_img = arreglo[pos].items[0].images[0].imageUrl;
    if (arreglo[pos].items[0].images.length > 2) {
      prod_imgc = arreglo[pos].items[0].images[2].imageUrl;
    } else {
      prod_imgc = arreglo[pos].items[0].images[1].imageUrl;
    }
    let prod_nombre = arreglo[0].productName;
    let tallas = [];

    for (let j = 0; j < arreglo[pos].items.length; j++) {
      let talla = arreglo[pos].items[j].variations[1].values[0];

      if (arreglo[pos].items[j].variations.length > 2) {
        talla = talla + arreglo[pos].items[j].variations[2].values[0];
      }
      if (arreglo[pos].items[j].sellers[0].commertialOffer.AvailableQuantity != 0) {
        itemid = arreglo[pos].items[j].itemId;

        nombre_prod = arreglo[pos].items[j].nameComplete;

        tallas.push(<input onClick={() => tallaActive()} class="kayserltda-arma-pack-2-x-btntalla" type="button" name={nombre_prod} value={talla} id={itemid} />)     }
    
    }

    ReactDOM.render(
      <fragment>
        <p className={`${pack.product}`}>{prod_nombre}</p>
        <img class="kayserltda-arma-pack-2-x-imgselected" src={prod_img} />

        <br />
        <div className={`${pack.contTallas}`} id="tallas">
          {tallas}
        </div>
        <br />
      </fragment>,
      document.getElementById('product')
    );
    ReactDOM.render(
      <fragment>
        <p className={`${pack.product}`}>{prod_nombre}</p>
        <img class="kayserltda-arma-pack-2-x-imgselected" src={prod_img} />

        <br />
        <div className={`${pack.contTallas}`} id="tallasm">
          {tallas}
        </div>
        <br />
      </fragment>,
      document.getElementById('productm')
    );

    
    ReactDOM.render(
      <fragment>
        <div className={`${pack.quality}`}>
          <div className={`${pack.containerMath}`}>
            <input className={`${pack.rest}`} onClick={() => lessqtty()} type="button" value="-" />
            <p className={`${pack.input}`}>1</p>
            <input className={`${pack.sum}`} onClick={() => addqtty()} type="button" value="+" />
          </div>
          <button className={`${pack.agregar}`} onClick={() => agregar(itemid, prod_imgc)} >Agregar</button>
        </div>
      </fragment>,
      document.getElementById('quantity')
    );
    ReactDOM.render(
      <fragment>
        <div className={`${pack.quality}`}>
          <div className={`${pack.containerMath}`}>
            <input className={`${pack.rest}`} onClick={() => lessqtty()} type="button" value="-" />
            <p className={`${pack.input}`}>1</p>
            <input className={`${pack.sum}`} onClick={() => addqtty()} type="button" value="+" />
          </div>
          <button className={`${pack.agregar}`} onClick={() => agregar(itemid, prod_imgc)} >Agregar</button>
        </div>
      </fragment>,
      document.getElementById('quantitym')
    );

    setTimeout(function(){ 
    let firstselect=$(".kayserltda-arma-pack-2-x-btntalla")
    console.log("first", firstselect) 
    firstselect[0].click()
      }, 500);
  }


   function lessqtty() {
  
    let input = $(".kayserltda-arma-pack-2-x-input")
    let agregado = parseInt(input[0].childNodes[0].textContent) - 1
    if (agregado < 0) {
      agregado = 0;
    }
    input[0].childNodes[0].textContent = agregado;
    let agregadom = parseInt(input[1].childNodes[0].textContent) - 1
    if (agregadom < 0) {
      agregadom = 0;
    }
    input[1].childNodes[0].textContent = agregadom;
  }

  function addqtty() {
    let input = $(".kayserltda-arma-pack-2-x-input")
    input[0].childNodes[0].textContent = parseInt(input[0].childNodes[0].textContent) + 1;
    input[1].childNodes[0].textContent = parseInt(input[1].childNodes[0].textContent) + 1;
    
  }


  let carrito = [];
  var packks = 1;
  function teFaltan() {

    let suma = 0;
    var carritodiv = $('#carrito');
    console.log(carritodiv)
    if (carritodiv[0].childElementCount == 0) {
      packks = 1;
      $(".kayserltda-arma-pack-2-x-btnaddcart").attr('style', 'display: none')

    }
    var carritocantprod = $('.kayserltda-arma-pack-2-x-prodCant');
    console.log("carritodiv", carritodiv[0].childNodes.length)
    for (let i = 0; i < carritodiv[0].childNodes.length; i++) {
      suma = suma + parseInt(carritocantprod[i].innerText)
    }

    console.log(cantPromo)
    let resta = cantPromo - suma

    if (resta < 1) {
      packks = Math.trunc(((resta * -1) / cantPromo) + 2)
      resta = resta + cantPromo * (packks - 1);
    }

    let faltan = $("#tefaltan")
    faltan.empty()
    faltan.append('<p class="kayserltda-arma-pack-2-x-textFaltan"> Te faltan ' + (resta) + ' para llevar ' + (packks) + ' pack</p>')

    let faltanm = $("#tefaltanm")
    faltanm.empty()
    faltanm.append('<p class="kayserltda-arma-pack-2-x-textFaltan"> Te faltan ' + (resta) + ' para llevar ' + (packks) + ' pack</p>')
  }


  function agregar(itemid, prod_img) {


    let input = $(".kayserltda-arma-pack-2-x-input")
    let cant = parseInt(input[0].childNodes[0].textContent)

    carrito.pop()
    var element = document.getElementById('tallas');
    var btns = element.getElementsByClassName("kayserltda-arma-pack-2-x-btntalla");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
      var current = document.getElementsByClassName(" kayserltda-arma-pack-2-x-active");
      itemid = current[0].id
      nombre_prod = current[0].name;
    }
    console.log(prod_img)
    carrito.push([cant, nombre_prod, itemid, prod_img])
    let carro = document.getElementById('carrito');
    let carro2 = document.getElementById('carritom');
    var prods = carro.getElementsByClassName("kayserltda-arma-pack-2-x-carritoProd");

    let validador = false;
    // Loop through the buttons and add the active class to the current/clicked button
    if (prods.length > 0) {
      for (var i = 0; i < prods.length; i++) {
        var actual = document.getElementsByClassName("kayserltda-arma-pack-2-x-prodCant");
        var codigo = document.getElementsByClassName("kayserltda-arma-pack-2-x-prodItemID");
        if (codigo[i].id == carrito[0][2]) {
          if (validador == false) {
            actual[i].textContent = actual[i].textContent.replace(actual[i].textContent, [carrito[0][0]]);
            validador = true;
            break;
          }
        }
      }
      if (validador == false) {
        carro.insertAdjacentHTML('beforeend',
          '<div class="kayserltda-arma-pack-2-x-carritoProd"  id="' + [carrito[0][2]] + '"><img class="kayserltda-arma-pack-2-x-imgCarro" src="' + (prod_img) + '" /><p class="kayserltda-arma-pack-2-x-prodCant">' + [carrito[0][0]] + '</p><p class="kayserltda-arma-pack-2-x-nomCarro">' + [carrito[0][1]] + '</p><p class="kayserltda-arma-pack-2-x-prodItemID" id="' + [carrito[0][2]] + '">' + [carrito[0][2]] + '</p><img class="kayserltda-arma-pack-2-x-imgBasu"  id="' + [carrito[0][2]] + '" src="/arquivos/Vector.png" /></div>');
          carro2.insertAdjacentHTML('beforeend',
          '<div class="kayserltda-arma-pack-2-x-carritoProd"  id="' + [carrito[0][2]] + '"><img class="kayserltda-arma-pack-2-x-imgCarro" src="' + (prod_img) + '" /><p class="kayserltda-arma-pack-2-x-prodCant">' + [carrito[0][0]] + '</p><p class="kayserltda-arma-pack-2-x-nomCarro">' + [carrito[0][1]] + '</p><p class="kayserltda-arma-pack-2-x-prodItemID" id="' + [carrito[0][2]] + '">' + [carrito[0][2]] + '</p><img class="kayserltda-arma-pack-2-x-imgBasu"  id="' + [carrito[0][2]] + '" src="/arquivos/Vector.png" /></div>');
          $(".kayserltda-arma-pack-2-x-btnaddcart").attr('style', '/*display: none*/')
      }
    }
    else {
        carro.insertAdjacentHTML('beforeend',
        '<div class="kayserltda-arma-pack-2-x-carritoProd"  id="' + [carrito[0][2]] + '"><img class="kayserltda-arma-pack-2-x-imgCarro" src="' + (prod_img) + '" /><p class="kayserltda-arma-pack-2-x-prodCant">' + [carrito[0][0]] + '</p><p class="kayserltda-arma-pack-2-x-nomCarro">' + [carrito[0][1]] + '</p><p class="kayserltda-arma-pack-2-x-prodItemID" id="' + [carrito[0][2]] + '">' + [carrito[0][2]] + '</p><img class="kayserltda-arma-pack-2-x-imgBasu"  id="' + [carrito[0][2]] + '" src="/arquivos/Vector.png" /></div>');
        carro2.insertAdjacentHTML('beforeend',
        '<div class="kayserltda-arma-pack-2-x-carritoProd"  id="' + [carrito[0][2]] + '"><img class="kayserltda-arma-pack-2-x-imgCarro" src="' + (prod_img) + '" /><p class="kayserltda-arma-pack-2-x-prodCant">' + [carrito[0][0]] + '</p><p class="kayserltda-arma-pack-2-x-nomCarro">' + [carrito[0][1]] + '</p><p class="kayserltda-arma-pack-2-x-prodItemID" id="' + [carrito[0][2]] + '">' + [carrito[0][2]] + '</p><img class="kayserltda-arma-pack-2-x-imgBasu"  id="' + [carrito[0][2]] + '" src="/arquivos/Vector.png" /></div>');
        $(".kayserltda-arma-pack-2-x-btnaddcart").attr('style', '/*display: none*/')
    }

    console.log(carrito)
    changeContainer2()

    $('body').on('click', '.kayserltda-arma-pack-2-x-imgBasu', function () {
      let idbasu = $(this).attr('id')
      let variab = $(".kayserltda-arma-pack-2-x-carritoProd")
      let idcarr = 0;
      for (let i = 0; i < variab.length; i++) {
        if (variab[i].id == idbasu) {
          idcarr = variab[i].id
        }
      }
      drop(idcarr, variab)
    })
    teFaltan()
    /* chargeQuery() */
  }

  function drop(id, divs) {

    for (let i = 0; i < divs.length; i++) {
      if (divs[i].id == id) {
        console.log(divs[i])
      }
    }
    let eliminado;
    let divscarrito = $(".kayserltda-arma-pack-2-x-carritoProd")
    for (let i = 0; i < divscarrito.length; i++) {
      if (divscarrito[i].id == id) {
        eliminado = divs[i];
      }
    }
    console.log("id", id, "div", divs)
    eliminado.remove()
    teFaltan()
  }


  function prueba2() {
    
    arreglo = newdata.data.products
    let largo = arreglo.length;
    let codigo = [];
    let prod_img;
    for (let i = 0; i < largo; i++) {
      if (arreglo[i].items[0].images.length > 2) {
        prod_img = arreglo[i].items[0].images[2].imageUrl;
      }
      else {
        prod_img = arreglo[i].items[0].images[0].imageUrl;
      }
      codigo.push(<img onClick={(e) => MostrarDetalle(i, e)} class="kayserltda-arma-pack-2-x-imgselection" src={prod_img} key={i} />)
    }

    ReactDOM.render(
      <div class="kayserltda-arma-pack-2-x-variants" id="variants">
        <MostrarVariantesProd codigo={codigo} />
      </div>, document.getElementById('section'));

  ReactDOM.render(
    <div class="kayserltda-arma-pack-2-x-variants" id="variantsm">
      <MostrarVariantesProd codigo={codigo} />
    </div>, document.getElementById('sectionm'));
  
  }

  function cargarQuery(id, e, cant) {
    e.preventDefault()
    setCollect(id);
    console.log("id cargar", id)
    let newdata = data;
    console.log("newdata cargar", newdata)
    setPromo(cant);
    loadGreeting();
  }

  const pack = useCssHandles(CSS_HANDLES)

  function HeadTipos() {
  
    
    return <div className={`${pack.genderBtnContainer}`}>
      {/* tipo de pack */}
      <button className={`${pack.btngenderH}`} onClick={e => Mostrar('H', e)}>HOMBRE</button>
      <button className={`${pack.btngenderM}`} onClick={e => Mostrar('M', e)}>MUJER</button>
    {/*   <button className={`${pack.btngenderK}`} onClick={e => Mostrar('K', e)}>TEEN AND KIDS</button> */}
    </div>
  }

  function SectionTipos(props) {
    let opciones
    switch (props.cod) {
      case 'M':
        opciones = <div className={`${pack.promosItem}`}>
{/*     <img className={`${pack.promosM}`} onClick={(e) => MostrarPromos("C", props.cod, e)} src={props.img1} alt="" />
*/}     <img className={`${pack.promosM}`} onClick={(e) => MostrarPromos("S", props.cod, e)} src={props.img2} alt="" />
        </div>
        break;
      case 'H':
        opciones = <div className={`${pack.promosItem}`}>
          <img className={`${pack.promosH}`} onClick={(e) => MostrarPromos("B", props.cod, e)} src={props.img1} alt="" />
          {/* <img className={`${pack.promosH}`} onClick={(e) => MostrarPromos("SL", props.cod, e)} src={props.img2} alt="" /> */}
        </div>
        break;
      /* case 'K':
        opciones = <div className={`${pack.promosItem}`}>
          <img className={`${pack.promosT}`} onClick={(e) => MostrarPromos("C", props.cod, e)} src={props.img1} alt="" />
          <img className={`${pack.promosT}`} onClick={(e) => MostrarPromos("S", props.cod, e)} src={props.img2} alt="" />
          <img className={`${pack.promosT}`} onClick={(e) => MostrarPromos("B", props.cod, e)} src={props.img3} alt="" />
          <img className={`${pack.promosT}`} onClick={(e) => MostrarPromos("SL", props.cod, e)} src={props.img4} alt="" />
        </div>
        break; */
    }
    return opciones;
  }

  function Mostrar(genero, e) {

    DeleteBack();
    e.preventDefault()
 
    let calzon = "/arquivos/normal-calzón.svg";
    let sosten = "/arquivos/normal-sostén.svg";
    let boxer = "/arquivos/normal-boxer.svg";
    let slip = "/arquivos/normal-slip.svg";

    switch (genero) {
      case "M":
        console.log("eligió mujer");
        ReactDOM.render(
          <SectionTipos img1={calzon} img2={sosten} cod={genero} />,
          document.getElementById('section')
        );
        ReactDOM.render(
          <SectionTipos img1={calzon} img2={sosten} cod={genero} />,
          document.getElementById('sectionm')
        );
        break;
      case "H":
        console.log("eligió hombre");
        ReactDOM.render(
          <SectionTipos img1={boxer} img2={slip} cod={genero} />,
          document.getElementById('section')
        );
        ReactDOM.render(
          <SectionTipos img1={boxer} img2={slip} cod={genero} />,
          document.getElementById('sectionm')
        );
        break;
      /* case "K":
        console.log("eligió kids");
        ReactDOM.render(
          <SectionTipos img1={calzon} img2={sosten} img3={boxer} img4={slip} cod={genero} />,
          document.getElementById('section')
        );
        ReactDOM.render(
          <SectionTipos img1={calzon} img2={sosten} img3={boxer} img4={slip} cod={genero} />,
          document.getElementById('sectionm')
        );
        break; */
    }
  }

  function DeleteBack() {

    ReactDOM.render(
      <div>
      </div>, document.getElementById('back'));
    ReactDOM.render(
      <div>
      </div>, document.getElementById('product'));
    ReactDOM.render(
      <div>
      </div>, document.getElementById('quantity'));

ReactDOM.render(
  <div>
  </div>, document.getElementById('backm'));
ReactDOM.render(
  <div>
  </div>, document.getElementById('productm'));
ReactDOM.render(
  <div>
  </div>, document.getElementById('quantitym'));
      

    let section = $('#section')

    if (section[0].childNodes[0].classList.value == "kayserltda-arma-pack-2-x-variants") {
      ReactDOM.render(
      <div></div>, document.getElementById('variants'));
      ReactDOM.render(
      <div></div>, document.getElementById('variantsm'));
    }
    $(".kayserltda-arma-pack-2-x-btnaddcart").attr('style', 'display: none')

    $("#tefaltan").empty();
    $("#tefaltanm").empty();
    $("#carrito").empty();
    $("#carritom").empty();
  }

  function Back(imgselect, genero) {
    const styles = {
      width: "45px",
      marginLeft: "7px",
    };
    ReactDOM.render(
      <div className={`${pack.back}`}>
        <img src={imgselect} style={styles} alt="" />
        <img onClick={(e) => Mostrar(genero, e)} className={`${pack.btnAzul}`} src="/arquivos/btn-return-azul.svg" alt="" />
      </div>, document.getElementById('back'));
  }
  function SectionPromos(props) {

    Back(props.imgselect, props.genero);

    let genero = props.genero;
    let tipo = props.tipo;

    switch (genero) {
      case "H":
        if (tipo == "B") {
          console.log("hombre-boxer");
          return <div className={`${pack.promos}`}>{/*522 para pruebas (más variedad de tallas)*/ }
            <div onClick={(e) => cargarQuery("534", e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$8.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('535', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$9.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
{/*             <div onClick={(e) => cargarQuery('536', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$11.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>*/} 
            <div onClick={(e) => cargarQuery('544', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$16.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
{/*             <div onClick={(e) => cargarQuery('545', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$22.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
 */}          </div>
        }
        else {
          console.log("hombre-slip");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('532', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$7.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        } 
        break;
      case "M":
        if (tipo == "C") {
          console.log("mujer-calzon");
          return <div className={`${pack.promos}`}>
{/*             <div onClick={(e) => cargarQuery('531', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$6.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
 */}            <div onClick={(e) => cargarQuery('532', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$7.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
{/*             <div onClick={(e) => cargarQuery('541', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$13.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
 */}            <div onClick={(e) => cargarQuery('542', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$14.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('549', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$20.699</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('550', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$20.699</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('555', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$21.599</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('556', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$23.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        else {
          console.log("mujer-sosten");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('530', e, 2)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x2.svg" alt="img-2X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$10.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('546', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$31.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('552', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$44.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('557', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$53.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        break;
      /* case "K":
        if (tipo == "C") {
          console.log("kids-calzon");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('531', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$6.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div> </div>
            <div onClick={(e) => cargarQuery('532', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$7.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('541', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$13.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('542', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$14.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('549', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$20.699</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('550', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$20.699</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('555', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$21.599</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('556', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$23.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        if (tipo == "S") {
          console.log("kids-sosten");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('530', e, 2)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x2.svg" alt="img-2X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$10.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('546', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$31.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('552', e, 9)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x9.svg" alt="img-9X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$44.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('557', e, 12)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x12.svg" alt="img-12X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$44.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        if (tipo == "B") {
          console.log("kids-boxer");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('534', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}> $8.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('535', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}> $9.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('536', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$11.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('544', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$16.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
            <div onClick={(e) => cargarQuery('545', e, 6)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x6.svg" alt="img-6X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$22.799</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        if (tipo == "SL") {
          console.log("kids-slip");
          return <div className={`${pack.promos}`}>
            <div onClick={(e) => cargarQuery('532', e, 3)} className={`${pack.promo}`}><img className={`${pack.X}`} src="/arquivos/x3.svg" alt="img-3X" /><div className={`${pack.leyenda}`}><p className={`${pack.price}`}>$7.999</p><p className={`${pack.descuento}`}>te ahorras $1000</p></div></div>
          </div>
        }
        break; */
    }
  }

  function MostrarPromos(tipo, genero, e) {
    e.preventDefault()

    let calzon = "/arquivos/btn-calzon-40px.svg";
    let sosten = "/arquivos/btn-sosten-40px.svg";
    let boxer = "/arquivos/btn-boxer-100px.svg";
    let slip = "/arquivos/btn-slip-40px.svg";

    switch (genero) {
      case "H":
        if (tipo == "B") {
          console.log("hombre-boxer");
          ReactDOM.render(
            <SectionPromos imgselect={boxer} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={boxer} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        else {
          console.log("hombre-slip");
          ReactDOM.render(
            <SectionPromos imgselect={slip} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={slip} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        break;
      case "M":
        if (tipo == "C") {
          console.log("mujer-calzon");
          ReactDOM.render(
            <SectionPromos imgselect={calzon} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={calzon} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        else {
          console.log("mujer-sosten");
          ReactDOM.render(
            <SectionPromos imgselect={sosten} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={sosten} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        break;
      /* case "K":
        if (tipo == "C") {
          console.log("kids-calzon");
          ReactDOM.render(
            <SectionPromos imgselect={calzon} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={calzon} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        if (tipo == "S") {
          console.log("kids-sosten");
          ReactDOM.render(
            <SectionPromos imgselect={sosten} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={sosten} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        if (tipo == "B") {
          console.log("kids-boxer");
          ReactDOM.render(
            <SectionPromos imgselect={boxer} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={boxer} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        if (tipo == "SL") {
          console.log("kids-slip");
          ReactDOM.render(
            <SectionPromos imgselect={slip} genero={genero} tipo={tipo} />,
            document.getElementById('section')
          );
          ReactDOM.render(
            <SectionPromos imgselect={slip} genero={genero} tipo={tipo} />,
            document.getElementById('sectionm')
          );
        }
        break; */
    }
  }

  function changeContainer(){
    $(".kayserltda-arma-pack-2-x-blockLeftMobile").attr('style', 'display: none')
    $(".kayserltda-arma-pack-2-x-blockCenterMobile").attr('style', 'display: flex')
    $(".kayserltda-arma-pack-2-x-blockRightMobile").attr('style', 'display: none')


  }

  function changeContainer2(){
    $(".kayserltda-arma-pack-2-x-blockCenterMobile").attr('style', 'display: none')
    $(".kayserltda-arma-pack-2-x-blockRightMobile").attr('style', 'display: flex')
    $(".kayserltda-arma-pack-2-x-blockLeftMobile").attr('style', 'display: none')
  }
  function changeContainer3(){
    $(".kayserltda-arma-pack-2-x-blockCenterMobile").attr('style', 'display: none')
    $(".kayserltda-arma-pack-2-x-blockRightMobile").attr('style', 'display: none')
    $(".kayserltda-arma-pack-2-x-blockLeftMobile").attr('style', 'display: flex')

  }


  return (<div className={`${pack.container}`}>
    {/* POP UP */}
    <div className={`${pack.containerprueba}`} style={{display: "none"}} >
      <div className={`${pack.msjepack}`} >
        <p className={`${pack.textPack}`}>¿Desea agregar otro pack?</p>
        <a href="/armatupack" className={`${pack.btnPopup}`}>SÍ</a>
        <a href="/" className={`${pack.btnPopup}`}>NO</a>
      </div>
    </div>
    
    {/* Versión Escritorio */}
    
    <div className={`${pack.containerDesktop}`}>
      {/* Column Left */}
      <div className={`${pack.blockLeft}`}>
        <div className={`${pack.pasoUnoContainer}`}> 
          {/* enunciado */}
          <p className={`${pack.textBlock}`}>Elige tu tipo de producto y pack</p>
          <p className={`${pack.circuloOn}`}>1</p>
        </div>
        <div id="gender" className={`${pack.genderContainer}`}>
          <HeadTipos />
        </div>  
        <div id="section" className={`${pack.sectionContainer}`}>
          <p className={`${pack.textProduct}`}>SELECCIONA TIPO DE PRODUCTO</p>
        </div>
        <div id="back" className={`${pack.backContainer}`}></div>
      </div>
      
      {/* column center */}
      <div className={`${pack.blockCenter}`}>
        <div className={`${pack.pasoDosContainer}`} > 
        {/* enunciado */}
          <p className={`${pack.textBlock}`}>Elige tu talla y cantidad</p>
          <p className={`${pack.circuloOff}`}>2</p>
        </div>
        <div id="packSelect"></div>
        <div id="product" className={`${pack.productContainer}`}></div>
        <div className={`${pack.containerTeFaltan}`} id="tefaltan"></div>
        <div className={`${pack.numero}`} id="quantity"></div>
      </div>
      
      {/* column right */}
      <div className={`${pack.blockRight}`}>
        <div className={`${pack.pasoTresContainer}`} > 
        {/* enunciado */}
          <p className={`${pack.textBlock}`}>Finaliza tu compra</p>
          <p className={`${pack.circuloOff}`}>3</p>
        </div>
            <p className={`${pack.textPedido}`}>TU PEDIDO</p>
            <div className={`${pack.carrito}`} id="carrito" >
            </div>
            <div className={`${pack.divBtnAddCart}`} id="btnadd">
            <input onClick={() => addToCart()} class="kayserltda-arma-pack-2-x-btnaddcart" type="button" value="COMPRAR" id="addToCart" style={{display: "none",}}/>       
        </div>
      </div>
    </div>

    {/* Versión Mobile */}

    <div className={`${pack.containerMobile}`}>
      {/* Column Left */}
      <div className={`${pack.blockLeftMobile}`} style={{display: "flex",}}>
      <div className={`${pack.pasosContainer}`}> 
          {/* Paso Uno */}
          <div className={`${pack.pasoUnoContainer}`}  onClick={() =>changeContainer3()}> 
            <p className={`${pack.circuloOn}`}>1</p>
            <p className={`${pack.textBlock}`}>Elige tu tipo de producto y pack</p>
          </div>
          {/* Paso Dos */}
          <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer()}> 
            <p className={`${pack.circuloOff}`}>2</p>
            <p className={`${pack.textBlock}`}>Elige tu talla y cantidad</p>    
          </div>
          {/* Paso Tres */}
          <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer2()}> 
            <p className={`${pack.circuloOff}`}>3</p>
            <p className={`${pack.textBlock}`}>Finaliza tu compra</p>
        </div>
        </div>
        <div className={`${pack.subBlockLeft}`}  name="mobile">
            <div id="genderm" className={`${pack.genderContainer}`} name="mobile">
            <HeadTipos />
            </div>  
            <div id="sectionm" className={`${pack.sectionContainsectionmer}`}>
              <p className={`${pack.textProduct}`}>SELECCIONA TIPO DE PRODUCTO</p>
            </div>
            <div id="backm" className={`${pack.backContainer}`}>
            </div>
        </div>
      </div>

      {/* column center */}
      <div className={`${pack.blockCenterMobile}`} style={{display: "none",}}>
          <div className={`${pack.pasosContainer}`}> 
            {/* Paso Uno */}
            <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer3()}> 
              <p className={`${pack.circuloOn}`}>1</p>
              <p className={`${pack.textBlock}`}>Elige tu tipo de producto y pack</p>
            </div>
            {/* Paso Dos */}
            <div className={`${pack.pasoDosContainer}`} onClick={() =>changeContainer()}> 
              <p className={`${pack.circuloOn}`}>2</p>
              <p className={`${pack.textBlock}`}>Elige tu talla y cantidad</p>    
            </div>
            {/* Paso Tres */}
            <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer2()}> 
              <p className={`${pack.circuloOff}`}>3</p>
              <p className={`${pack.textBlock}`}>Finaliza tu compra</p>
            </div>
          </div>
          <div className={`${pack.subBlockCenter}`}>
            <div id="packSelect"></div>
            <div id="productm" className={`${pack.productContainer}`}></div>
            <div className={`${pack.containerTeFaltan}`} id="tefaltanm"></div>
            <div className={`${pack.numero}`} id="quantitym"></div>
          </div>
      </div>
      
      
      {/* column right */}
      <div className={`${pack.blockRightMobile}`} style={{display: "none",}}>
      <div className={`${pack.pasosContainer}`}> 
            {/* Paso Uno */}
            <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer3()}> 
              <p className={`${pack.circuloOff}`}>1</p>
              <p className={`${pack.textBlock}`}>Elige tu tipo de producto y pack</p>
            </div>
            {/* Paso Dos */}
            <div className={`${pack.pasoOffContainer}`} onClick={() =>changeContainer()}> 
              <p className={`${pack.circuloOff}`}>2</p>
              <p className={`${pack.textBlock}`}>Elige tu talla y cantidad</p>    
            </div>
            {/* Paso Tres */}
            <div className={`${pack.pasoTresContainer}`} onClick={() =>changeContainer2()}> 
              <p className={`${pack.circuloOn}`}>3</p>
              <p className={`${pack.textBlock}`}>Finaliza tu compra</p>
            </div>
          </div>
        <div className={`${pack.subBlockRight}`}>
            <p className={`${pack.textPedido}`}>TU PEDIDO</p>
            <div className={`${pack.carrito}`} id="carritom" >
            </div>
            <div className={`${pack.divBtnAddCart}`} id="btnaddm">
            <input onClick={() => addToCart()} class="kayserltda-arma-pack-2-x-btnaddcart" type="button" value="COMPRAR" id="addToCart" style={{display: "none",}}/>       
            </div>
        </div>
      </div>
    </div>


    </div>
  )
}

export default pack2